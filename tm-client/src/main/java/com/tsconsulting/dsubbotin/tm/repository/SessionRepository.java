package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.ISessionRepository;
import com.tsconsulting.dsubbotin.tm.endpoint.Session;
import org.jetbrains.annotations.Nullable;

public class SessionRepository implements ISessionRepository {

    @Nullable
    private Session session;

    @Nullable
    public Session getSession() {
        return session;
    }

    public void setSession(@Nullable final Session session) {
        this.session = session;
    }

}
