package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.endpoint.Session;
import org.jetbrains.annotations.Nullable;

public interface ISessionService {

    @Nullable
    Session getSession();

    void setSession(@Nullable final Session session);

}
