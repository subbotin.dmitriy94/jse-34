package com.tsconsulting.dsubbotin.tm.command.user.admin;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.Session;
import com.tsconsulting.dsubbotin.tm.endpoint.User;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserShowByIdCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return "user-show-by-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Display user by id.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable Session session = endpointLocator.getSessionService().getSession();
        TerminalUtil.printMessage("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final User user = endpointLocator
                .getAdminUserEndpoint()
                .findByIdUser(session, id);
        showUser(user);
    }

}
