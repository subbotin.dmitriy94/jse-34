package com.tsconsulting.dsubbotin.tm.command;

import com.tsconsulting.dsubbotin.tm.endpoint.Task;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTask(@NotNull final Task task) {
        TerminalUtil.printMessage("Id: " + task.getId() + "\n" +
                "Name: " + task.getName() + "\n" +
                "Project id: " + task.getProjectId() + "\n" +
                "Description: " + task.getDescription() + "\n" +
                "Status: " + task.getStatus().value() + "\n" +
                "Create date: " + task.getCreateDate() + "\n" +
                "Start date: " + task.getStartDate()
        );
    }

    protected String showTaskLine(@NotNull final Task task) {
        return String.format("%s - User Id: %s; Project Id: %s; Name: %s; Created: %s; Status: %s; Started: %s;",
                task.getId(),
                task.getUserId(),
                task.getProjectId(),
                task.getName(),
                task.getCreateDate(),
                task.getStatus(),
                task.getStartDate());
    }

}
