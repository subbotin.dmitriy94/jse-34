package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.Session;
import com.tsconsulting.dsubbotin.tm.endpoint.Task;
import com.tsconsulting.dsubbotin.tm.enumerated.Sort;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;

public final class TaskListShowCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-list";
    }

    @Override
    @NotNull
    public String description() {
        return "Display task list.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable Session session = endpointLocator.getSessionService().getSession();
        TerminalUtil.printMessage("Enter sort:");
        TerminalUtil.printMessage(Arrays.toString(Sort.values()));
        @NotNull final String sort = TerminalUtil.nextLine();
        List<Task> tasks = endpointLocator.getTaskEndpoint().findAllTask(session, sort);
        int index = 1;
        for (@NotNull final Task task : tasks) TerminalUtil.printMessage(index++ + ". " + showTaskLine(task));
    }

}
