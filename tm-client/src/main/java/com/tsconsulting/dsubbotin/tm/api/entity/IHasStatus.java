package com.tsconsulting.dsubbotin.tm.api.entity;

import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import org.jetbrains.annotations.NotNull;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}
