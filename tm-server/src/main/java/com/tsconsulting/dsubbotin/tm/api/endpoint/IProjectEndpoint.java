package com.tsconsulting.dsubbotin.tm.api.endpoint;

import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.model.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @WebMethod
    boolean existByIdProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException;

    @WebMethod
    void addProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entity") Project entity
    ) throws AbstractException;

    @WebMethod
    void removeProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entity") Project entity
    ) throws AbstractException;

    @NotNull
    @WebMethod
    List<Project> findAllProject(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "sort") String sort
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Project findByIdProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Project findByIndexProject(
            @Nullable @WebParam(name = "session") Session session,
            @WebParam(name = "index") int index
    ) throws AbstractException;

    @WebMethod
    void removeByIdProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException;

    @WebMethod
    void removeByIndexProject(
            @Nullable @WebParam(name = "session") Session session,
            @WebParam(name = "index") int index
    ) throws AbstractException;

    @WebMethod
    void createProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "description") String description
    ) throws AbstractException;

    @NotNull

    @WebMethod
    Project findByNameProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "name") String name
    ) throws AbstractException;

    @WebMethod
    void updateByIdProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id,
            @NotNull @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "description") String description
    ) throws AbstractException;

    @WebMethod
    void updateByIndexProject(
            @Nullable @WebParam(name = "session") Session session,
            @WebParam(name = "index") int index,
            @NotNull @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "description") String description
    ) throws AbstractException;

    @WebMethod
    void startByIdProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException;

    @WebMethod
    void startByIndexProject(
            @Nullable @WebParam(name = "session") Session session,
            @WebParam(name = "index") int index
    ) throws AbstractException;

    @WebMethod
    void startByNameProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "name") String name
    ) throws AbstractException;

    @WebMethod
    void finishByIdProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException;

    @WebMethod
    void finishByIndexProject(
            @Nullable @WebParam(name = "session") Session session,
            @WebParam(name = "index") int index
    ) throws AbstractException;

    @WebMethod
    void finishByNameProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "name") String name
    ) throws AbstractException;

    @WebMethod
    void updateStatusByIdProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id,
            @NotNull @WebParam(name = "status") Status status
    ) throws AbstractException;

    @WebMethod
    void updateStatusByIndexProject(
            @Nullable @WebParam(name = "session") Session session,
            @WebParam(name = "index") int index,
            @NotNull @WebParam(name = "status") Status status
    ) throws AbstractException;

    @WebMethod
    void updateStatusByNameProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "status") Status status
    ) throws AbstractException;

}
