package com.tsconsulting.dsubbotin.tm.exception;

import javax.xml.ws.WebFault;

@WebFault(name = "AbstractException")
public abstract class AbstractException extends java.lang.Exception {

    public AbstractException() {
        super();
    }

    public AbstractException(String message) {
        super(message);
    }

    public AbstractException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractException(Throwable cause) {
        super(cause);
    }

}
