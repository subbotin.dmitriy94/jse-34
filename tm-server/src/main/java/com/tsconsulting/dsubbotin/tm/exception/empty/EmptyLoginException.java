package com.tsconsulting.dsubbotin.tm.exception.empty;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;


public final class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Empty login entered.");
    }

}
