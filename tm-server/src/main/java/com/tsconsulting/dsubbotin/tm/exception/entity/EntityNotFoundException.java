package com.tsconsulting.dsubbotin.tm.exception.entity;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;

import javax.xml.ws.WebFault;

@WebFault(name = "EntityNotFoundException")
public final class EntityNotFoundException extends AbstractException {

    public EntityNotFoundException() {
        super("Entity not found!");
    }

}