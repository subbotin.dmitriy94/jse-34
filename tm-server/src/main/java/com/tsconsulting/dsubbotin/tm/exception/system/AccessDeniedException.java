package com.tsconsulting.dsubbotin.tm.exception.system;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;

import javax.xml.ws.WebFault;

@WebFault(name = "AccessDeniedException")
public final class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Access denied!");
    }

}
